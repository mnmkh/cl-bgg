(in-package :cl-user)

(defpackage :cl-bgg-asd
  (:use :cl :asdf))

(in-package :cl-bgg-asd)

(asdf:defsystem :cl-bgg
    :description "boardgamegeek api for common lisp."
    :version "0.1"
    :author "mnmkh"
    :serial T
    :components ((:file "packages")
                 (:file "config")
                 (:file "cl-bgg")
                 (:file "api2"))
    :depends-on (:skip
		 :cl-ppcre
		 :drakma
		 :cxml
		 :cxml-stp
		 :xpath
                 :fiveam))

(defmethod asdf:perform ((op asdf:test-op) (system (eql (asdf:find-system "cl-bgg"))))
  (funcall (intern (string :run!) (string :it.bese.FiveAM)) cl-bgg))
