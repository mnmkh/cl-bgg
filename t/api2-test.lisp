;;;; Author: mnmkh
(in-package :cl-bgg)

;;;; Description.
(defparameter *catan* 
  (boardgame2 "13" :parameters '(("ratingcomments" . "1"))))

(defparameter *item* 
  (car (bg2-items-of *catan*)))

;; test main
(fiveam:def-suite api2-boardgame-test :description "api2-boardgame-test")
(fiveam:in-suite api2-boardgame-test)

(fiveam:test guid-test
  (fiveam:is (string= "13" (bg2-guid-of *item*))))

(fiveam:test thumbnail-of
  (fiveam:is (string= "http://cf.geekdo-images.com/images/pic268839_t.jpg" (bg2-thumbnail-of *item*))))

(fiveam:test image-of
  (fiveam:is (string= "http://cf.geekdo-images.com/images/pic268839.jpg" (bg2-image-of *item*))))

(fiveam:test name-of
  (fiveam:is (string= "The Settlers of Catan" (bg2-name-of *item*))))

(fiveam:test description-of
  (fiveam:is (string= "In Settlers of Catan, players try to be the dominant force on the island of Catan by building settlements, cities, and roads.  On each turn dice are rolled to determine what resources the island produces.  Players collect these resources to build up their civilizations to get to 10 victory points and win the game.  Multi-award-winning and one of the most popular games in recent history due to its amazing ability to appeal to non-gamers and gamers alike.&#10;&#10;Die Siedler von Catan was originally published by Kosmos and has gone through multiple editions. It was licensed by Mayfair and has undergone at least 4 editions as Settlers of Catan. It has also been re-published in a travel edition by Kosmos and in yet another edition for Japan/Asia, Settlers of Catan: Rockman Edition.&#10;&#10;Settlers of Catan is the original game in the Catan Series.&#10;&#10;" (bg2-description-of *item*))))

(fiveam:test yearpublished-of
  (fiveam:is (string= "1995" (bg2-yearpublished-of *item*))))

(fiveam:test minplayers-of
  (fiveam:is (string= "3" (bg2-minplayers-of *item*))))

(fiveam:test maxplayers-of
  (fiveam:is (string= "4" (bg2-maxplayers-of *item*))))

(fiveam:test playingtime-of
  (fiveam:is (string= "90" (bg2-playingtime-of *item*))))

(fiveam:test age-of
  (fiveam:is (string= "10" (bg2-age-of *item*))))

(fiveam:test boardgamecategory-of
  (fiveam:is (equal (list "Negotiation") 
		    (bg2-boardgamecategory-of *item*))))

(fiveam:test boardgamemechanic-of
  (fiveam:is (equal (list "Dice Rolling" "Hand Management" "Modular Board" "Route/Network Building" "Trading")
		    (bg2-boardgamemechanic-of *item*))))

(fiveam:test boardgamefamily-of
  (fiveam:is (equal (list "Catan" "Promotional Board Games")
		    (bg2-boardgamefamily-of *item*))))

(fiveam:test boardgamedesigner-of
  (fiveam:is (equal (list "Klaus Teuber")
		    (bg2-boardgamedesigner-of *item*))))

(fiveam:test boardgameartist-of
  (fiveam:is (string= "Volkan Baga"
		      (car (bg2-boardgameartist-of *item*)))))

(fiveam:test boardgameexpansion-of
  (fiveam:is (string= "Atlantis: Szenarien & Varianten zu Die Siedler von Catan"
		      (car (bg2-boardgameexpansion-of *item*)))))

;(fiveam:test boardgameversion-of
;  (fiveam:is (equal (list "Klaus Teuber")
;		    (bg2-boardgamedesigner-of *item*))))

(fiveam:test boardgamepublisher-of
  (fiveam:is (string= "999 Games"
		      (car (bg2-boardgamepublisher-of *item*)))))

(fiveam:test comments-of
  (fiveam:is (string= "comment" (car (bg2-comments-of *item*)))))

(fiveam:run! 'api2-boardgame-test)
