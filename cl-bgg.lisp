(in-package :cl-bgg)

; *minimum-query-interval* [ms]
; for block continuous access.
(defvar *minimum-query-interval* 5)
(defparameter *last-query-time* (get-universal-time))
(defun wait-query ()
  (let ((wait (- (+ *last-query-time* *minimum-query-interval*) (get-universal-time))))
    (when (> wait 0)
      (sleep wait))))

(defvar *content-type-xml* "text/xml;")
(defun content-type-xmlp (header)
  (scan-to-strings *content-type-xml* (cdr (assoc :content-type header))))

(defun xml-to-sxml (xml)
  (cxml:parse xml (cxml-xmls:make-xmls-builder)))

(defun get-nodes-from-sxml (nodename sxml)
  (when (and nodename sxml)
    (let ((xpath:*navigator* (cxml-xmls:make-xpath-navigator))
	  (context (concatenate 'string "//" nodename)))
      (xpath:all-nodes (xpath:evaluate context sxml)))))

(defun xml-get-request (url parameters)
  (wait-query)
  (multiple-value-bind (body-or-stream status-code  headers  uri  stream  must-close  reason-phrase)
      (http-request url :method :get :parameters parameters :external-format-out :utf-8)
    (progn (setf *last-query-time* (get-universal-time))
	   (when (and (= status-code 200) (content-type-xmlp headers))
	     body-or-stream))))

;;
;; rss utility
;;
(defun get-rss-feeds (&key (url *rss-url*) (parameters nil))
  (xml-get-request url parameters))

(defun get-newgame-list ()
  (let ((get-parameters (list '("infilters[0]" . "thing" ) '("domain" . "boardgame"))))
    (awhen (get-rss-feeds :parameters get-parameters)
      (awhen (xml-to-sxml it)
	(get-nodes-from-sxml "item" it)))))

;;
;; xmlapi command
;;
(defun xmlapi (cmd &key (url *xmlapi-url*) (parameters nil))
  (xml-get-request (concatenate 'string url cmd) parameters))

(defun xmlapi2 (cmd &key (url *xmlapi2-url*) (parameters nil))
  (xml-get-request (concatenate 'string url cmd) parameters))

;;
;; xml boardgame command
;; 
(defun boardgame (guid &key (parameters nil) (result-type :xmls))
  (let ((cmd (concatenate 'string "boardgame/" guid)))
    (awhen (xmlapi cmd :parameters parameters)
      (cond ((eql result-type :xml) it)
	    (t (xml-to-sxml it))))))

(defun guid-of (sxml)
  (second (caadar (get-nodes-from-sxml "boardgame" sxml))))

(defun yearpublished-of (sxml)
  (third (car (get-nodes-from-sxml "yearpublished" sxml))))

(defun minplayers-of (sxml)
  (third (car (get-nodes-from-sxml "minplayers" sxml))))

(defun maxplayers-of (sxml) 
  (third (car (get-nodes-from-sxml "maxplayers" sxml))))

(defun playingtime-of (sxml) 
  (third (car (get-nodes-from-sxml "playingtime" sxml))))

(defun age-of (sxml) 
  (third (car (get-nodes-from-sxml "age" sxml))))

(defun primary-name-of (sxml)
  (loop for name-value in (get-nodes-from-sxml "name" sxml)
     when (find "primary" (second name-value) :key #'car :test #'string=)
       collect (third name-value)))

(defun name-of (sxml) 
  (car (primary-name-of sxml)))

(defun thumbnail-of (sxml)
  (third (car (get-nodes-from-sxml "thumbnail" sxml))))

(defun image-of (sxml)
  (third (car (get-nodes-from-sxml "image" sxml))))

(defun description-of (sxml)
  (third (car (get-nodes-from-sxml "description" sxml))))

(defun boardgamemechanic-of (sxml)
  (map 'list (lambda (i) (third i)) (get-nodes-from-sxml "boardgamemechanic" sxml)))

(defun boardgamecategory-of (sxml)
  (map 'list (lambda (i) (third i)) (get-nodes-from-sxml "boardgamecategory" sxml)))

(defun boardgameversion-of (sxml)
  (map 'list (lambda (i) (third i)) (get-nodes-from-sxml "boardgameversion" sxml)))

(defun boardgamedesigner-of (sxml)
  (map 'list (lambda (i) (third i)) (get-nodes-from-sxml "boardgamedesigner" sxml)))

(defun boardgamepublisher-of (sxml)
  (map 'list (lambda (i) (third i)) (get-nodes-from-sxml "boardgamepublisher" sxml)))

(defun boardgamefamily-of (sxml)
  (map 'list (lambda (i) (third i)) (get-nodes-from-sxml "boardgamefamily" sxml)))

(defun usersrated-of (sxml)
  (get-nodes-from-sxml "usersrated" sxml))

(defun usersrated-value-of (usersrated-sxml)
  (third (car usersrated-sxml)))

(defun comments-of (sxml)
  (get-nodes-from-sxml "comment" sxml))

(defun comment-value-of (comment-sxml)
  (cadr (first (cadr comment-sxml))))

(defun comment-rating-of (comment-sxml)
  (cadr (second (cadr comment-sxml))))

(defun comment-username-of (comment-sxml)
  (cadr (third (cadr comment-sxml))))

;;
;; xml collection command
;; 
(defun collection (name &key (parameters nil) (result-type :xmls))
  (let ((cmd (concatenate 'string "collection/" (cl-ppcre:regex-replace-all " " name "%20"))))
    (awhen (xmlapi cmd :parameters parameters)
      (cond ((eql result-type :xml) it)
	    (t (xml-to-sxml it))))))

(defun items-of (sxml)
  (get-nodes-from-sxml "item" sxml))

(defun item-value-of (item-sxml)
  (cadr item-sxml))

(defun item-guid-of (item-sxml)
  "return guid from item sxml. support object type thing and boardgame."
  (ifbind objectid (get-nodes-from-sxml "objectid" (item-value-of item-sxml))
	  (second (car objectid))                    ; objectid
	  (second (car (item-value-of item-sxml))))) ; id

(defun item-stats-of (item-sxml)
  (car (get-nodes-from-sxml "stats" item-sxml)))

(defun item-stats-value-of (item-sxml)
  (cadr (item-stats-of "stats" item-sxml)))

(defun item-stats-rating-of (item-sxml)
  (car (get-nodes-from-sxml "rating" (item-stats-of item-sxml))))

(defun item-stats-rating-value-of (item-sxml)
  (cadr (item-stats-rating-of item-sxml)))

(defun item-rating-of (item-sxml)
  (second (car (item-stats-rating-value-of item-sxml))))

(defun item-status-of (item-sxml)
  (car (get-nodes-from-sxml "status" item-sxml)))

(defun item-status-value-of (item-sxml)
  (cadr (item-status-of item-sxml)))

(defun item-own-of (item-sxml)
  (second (car (get-nodes-from-sxml "own" (item-status-value-of item-sxml)))))

(defun item-wanttobuy-of (item-sxml)
  (second (car (get-nodes-from-sxml "wanttobuy" (item-status-value-of item-sxml)))))

(defun item-wanttoplay-of (item-sxml)
  (second (car (get-nodes-from-sxml "wanttoplay" (item-status-value-of item-sxml)))))

;;
;; xml search cmd
;;
(defun search-boardgame (val &key (exact "0") (result-type :xmls))
  (let ((parameters (list (cons "search" val) (cons "exact" exact))))
    (awhen (xmlapi "search" :parameters parameters)
      (cond ((eql result-type :xml) it)
	    (t (xml-to-sxml it))))))

(defun exact-search-valid-lengthp (val)
  (> (length val) 2))

(defun boardgames-of (sxml)
  (get-nodes-from-sxml "boardgame" sxml))

(defun boardgames-guid-of (sxml)
  (loop for node in (get-nodes-from-sxml "boardgame" sxml)
     collect (second (caadr node))))

(defun boardgame-guid-of (boardgame-sxml)
  (second (caadr boardgame-sxml)))

(defun boardgame-name-of (boardgame-sxml)
  (third (car (get-nodes-from-sxml "name" boardgame-sxml))))
