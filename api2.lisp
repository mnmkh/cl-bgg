;;;; Author: mnmkh
(in-package :cl-bgg)

;;;; Boardgamegeek xmlapi2 function.
(defun xmlapi2 (cmd &key (url *xmlapi2-url*) (parameters nil))
  (xml-get-request (concatenate 'string url cmd) parameters))

(defun boardgame2 (guid &key (parameters nil) (result-type :xmls))
  (let ((cmd (concatenate 'string "thing?type=boardgame,boardgameexpansion&id=" guid)))
    (awhen (xmlapi2 cmd :parameters parameters)
      (cond ((eql result-type :xml) it)
	    (t (xml-to-sxml it))))))

(defun get-link-nodes-from-sxml (type sxml)
  (when (and type sxml)
    (let ((xpath:*navigator* (cxml-xmls:make-xpath-navigator))
	  (context (concatenate 'string "//link[@type='" type "']")))
      (xpath:all-nodes (xpath:evaluate context sxml)))))

(defmacro get-first-value-from-node (node)
  `(cadr (first (cadr ,node))))

(defmacro get-second-value-from-node (node)
  `(cadr (second (cadr ,node))))

(defmacro get-third-value-from-node (node)
  `(cadr (third (cadr ,node))))

(defmacro get-first-value-from-noattr-node (node)
  `(caddr ,node))

(defun bg2-items-of (sxml)
  (get-nodes-from-sxml "item" sxml))

(defun bg2-item-value-of (item-sxml)
  (cadr item-sxml))

(defun bg2-guid-of (sxml)
  (second (car (item-value-of sxml)))) ;id

(defun bg2-primary-name-of (sxml)
  (get-first-value-from-node (car (get-nodes-from-sxml "name[@type='primary']" sxml))))

(defun bg2-alternate-names-of (sxml)
  (loop for name-value in (get-nodes-from-sxml "name[@type='alternate']" sxml)
     collect (get-first-value-from-node name-value)))

(defun bg2-names-of (sxml)
  (loop for name-value in (get-nodes-from-sxml "name" sxml)
     collect (get-first-value-from-node name-value)))

(defun bg2-name-of (sxml) 
  (bg2-primary-name-of sxml))

(defun bg2-thumbnail-of (sxml)
  (get-first-value-from-noattr-node (car (get-nodes-from-sxml "thumbnail" sxml))))

(defun bg2-image-of (sxml)
  (get-first-value-from-noattr-node (car (get-nodes-from-sxml "image" sxml))))

(defun bg2-description-of (sxml)
  (get-first-value-from-noattr-node (car (get-nodes-from-sxml "description" sxml))))

(defun bg2-yearpublished-of (sxml)
  (get-first-value-from-node (car (get-nodes-from-sxml "yearpublished" sxml))))

(defun bg2-minplayers-of  (sxml)
  (get-first-value-from-node (car (get-nodes-from-sxml "minplayers" sxml))))

(defun bg2-maxplayers-of  (sxml)
  (get-first-value-from-node (car (get-nodes-from-sxml "maxplayers" sxml))))

(defun bg2-playingtime-of  (sxml)
  (get-first-value-from-node (car (get-nodes-from-sxml "playingtime" sxml))))

(defun bg2-age-of  (sxml)
  (get-first-value-from-node (car (get-nodes-from-sxml "minage" sxml))))

(defun bg2-minage-of  (sxml)
  (bg2-age-of sxml))

(defun bg2-boardgamecategory-of (sxml)
  (map 'list (lambda (i) (get-first-value-from-node i)) (get-link-nodes-from-sxml "boardgamecategory" sxml)))

(defun bg2-boardgamemechanic-of (sxml)
  (map 'list (lambda (i) (get-first-value-from-node i)) (get-link-nodes-from-sxml "boardgamemechanic" sxml)))

(defun bg2-boardgamefamily-of (sxml)
  (map 'list (lambda (i) (get-first-value-from-node i)) (get-link-nodes-from-sxml "boardgamefamily" sxml)))

(defun bg2-boardgameexpansion-of (sxml)
  (map 'list (lambda (i) (get-first-value-from-node i)) (get-link-nodes-from-sxml "boardgameexpansion" sxml)))

(defun bg2-boardgamedesigner-of (sxml)
  (map 'list (lambda (i) (get-first-value-from-node i)) (get-link-nodes-from-sxml "boardgamedesigner" sxml)))

(defun bg2-boardgameartist-of (sxml)
  (map 'list (lambda (i) (get-first-value-from-node i)) (get-link-nodes-from-sxml "boardgameartist" sxml)))

(defun bg2-boardgameversion-of (sxml)
  (map 'list (lambda (i) (get-first-value-from-node i)) (get-link-nodes-from-sxml "boardgameversion" sxml)))

(defun bg2-boardgamepublisher-of (sxml)
  (map 'list (lambda (i) (get-first-value-from-node i)) (get-link-nodes-from-sxml "boardgamepublisher" sxml)))

(defun bg2-comments-of (sxml)
  (get-nodes-from-sxml "comments/comment" sxml))

(defun bg2-comment-rating-of (comment)
  (get-second-value-from-node comment))

(defun bg2-comment-username-of (comment)
  (get-third-value-from-node comment))
