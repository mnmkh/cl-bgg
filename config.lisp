(in-package :cl-bgg)

; api url
(defvar *xmlapi-url* "http://www.boardgamegeek.com/xmlapi/")
(defvar *xmlapi2-url* "http://www.boardgamegeek.com/xmlapi2/")
(defvar *rss-url* "http://boardgamegeek.com/recentadditions/rss")
