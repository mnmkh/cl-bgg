(in-package :cl-user)

(defpackage :cl-bgg
  (:nicknames :bgg)
  (:use :cl
	:skip
	:cl-ppcre
	:drakma)
  (:export :get-newgame-list
	   :get-nodes-from-sxml
	   ;; boardgame cmd
	   :boardgame
	   :guid-of
	   :yearpublished-of
	   :minplayers-of
	   :maxplayers-of
	   :playingtime-of
	   :age-of
	   :name-of
	   :thumbnail-of
	   :image-of
	   :description-of
	   :boardgamemechanic-of
	   :boardgamecategory-of
	   :boardgameversion-of
	   :boardgamedesigner-of
	   :boardgamepublisher-of
	   :boardgamefamily-of
	   :usersrated-of
	   :usersrated-value-of
	   :comments-of
	   :comment-value-of
	   :comment-rating-of
	   :comment-username-of
	   ;; collection cmd
	   :collection
	   :items-of
	   :item-guid-of
	   :item-rating-of
	   :item-own-of
	   :item-wanttobuy-of
	   :item-wanttoplay-of
	   ;; search 
	   :search-boardgame
	   :exact-search-valid-lengthp
	   :boardgames-of
	   :boardgames-guid-of
	   ;; api2
	   :get-link-nodes-from-sxml
	   :get-first-value-from-node
	   :get-first-value-from-noattr-node
	   ;; boardgame2
	   :boardgame2
	   :bg2-items-of
	   :bg2-item-value-of
	   :bg2-guid-of
	   :bg2-primary-name-of
	   :bg2-alternate-names-of
	   :bg2-names-of
	   :bg2-name-of
	   :bg2-thumbnail-of
	   :bg2-image-of
	   :bg2-name-of
	   :bg2-description-of
	   :bg2-yearpublished-of
	   :bg2-minplayers-of
	   :bg2-maxplayers-of
	   :bg2-playingtime-of
	   :bg2-age-of
	   :bg2-minage-of
	   :bg2-boardgamecategory-of
	   :bg2-boardgamemechanic-of
	   :bg2-boardgamefamily-of
	   :bg2-boardgameexpansion-of
	   :bg2-boardgamedesigner-of
	   :bg2-boardgameartist-of
	   :bg2-boardgameversion-of
	   :bg2-boardgamepublisher-of
	   :bg2-comments-of
	   :bg2-comment-rating-of
	   :bg2-comment-username-of))
